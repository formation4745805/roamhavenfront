import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
        "banner": "url('/final-version-banner.jpeg')",
      },
      backgroundPosition: {
        bottom: 'bottom',
        'bottom-4': 'center bottom 1rem',
        center: 'center',
        'center-top-4':'center center 1',
        left: 'left',
        'left-bottom': 'left bottom',
        'left-top': 'left top',
        right: 'right',
        'right-bottom': 'right bottom',
        'right-top': 'right top',
        top: 'top',
        'top-4': 'center top 1rem',
        'top-8': 'center top 5rem',
      },
      boxShadow: {
        'verte': '0 15px 32px -9px rgba(3, 140, 115, 0.4)',
        'noiratre': '0 10px 22px -6px rgba(2, 32, 38, 0.4)',
      },
    },
    colors: {
      vertBlack:"#022026",
      vertFonce: "#038c73",
      vertClaire: "#73d9b3",
      jaunatre: "#f2f0d8",
      blanchatre: "#f2f2f2",
      white: "#fff"
    },
  },
  plugins: [],
};
export default config;
