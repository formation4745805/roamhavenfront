'use client'

import { Footer } from "@/app/Components/footer"
import { Header } from "@/app/Components/header"
import { formulaireRegister } from "@/app/Services/formulaire"
import { getThisVoyageById, getThisVoyageByName } from "@/app/Services/voyage"
import { showProps } from "@/app/Utils/types"
import { useRouter } from "next/navigation"
import { useEffect, useState } from "react"
import { MagnifyingGlass } from "react-loader-spinner"


const Page = ({ params }: { params: { voyageId: string } }) => {

    const [voyageData, setVoyageData] = useState<showProps>()
    const [isLoading, setIsLoading] = useState(false)
    const {push} = useRouter()

    const [nom, setNom] = useState('')
    const [prenom, setPrenom] = useState('')
    const [email, setEmail] = useState('')
    const [tel, setTel] = useState('')
    const [message, setMessage] = useState('')

    useEffect(() => {

        setIsLoading(true)

        getThisVoyageById(params.voyageId).then((res) => {
            setVoyageData(res)
            setIsLoading(false)
        })
    }, [])

    function handleSubmit() {
        if (
            !nom ||
            !prenom ||
            !email ||
            !tel ||
            !message
        ) {
            alert('Certains champs sont manquants!')
        } else {
            let formulaireData = {
                nom: nom,
                prenom: prenom,
                email: email,
                tel: tel,
                message: message,
            }
            formulaireRegister(formulaireData).then((res) => {
                if (res.status === 201) {
                    push('/')
                }
            })
        }
    }

    return (
        <div className="flex min-h-screen flex-col items-center justify-between bg-white">
            < Header />
            {isLoading && (
                <div className="w-full h-full flex items-center justify-center">
                    < MagnifyingGlass
                      visible={true}
                      height="80"
                      width="80"
                      ariaLabel="magnifying-glass-loading"
                      wrapperStyle={{}}
                      wrapperClass="magnifying-glass-wrapper"
                      glassColor="#c0efff"
                      color="#e15b64"
                      />
                </div>)}
            {voyageData && (
                <div
                key={voyageData.id}
                className="m-6 w-11/12 lg:w-2/3 2xl:w-10/12 rounded-md flex flex-col items-start justify-center bg-blanchatre p-2 lg:p-4 shadow-noiratre"
                >
                    <h2 className="w-full text-center justify-center font-bold text-2xl 2xl:text-3xl m-4 py-1 drop-shadow">Prise de contact</h2>
                    <p className="w-3/4 flex justify-center items-center 2xl:text-lg m-1">Vous souhaitez nous contacter concernant le voyage :</p>
                    <p className="w-full text-center font-bold justify-center text-xl 2xl:text-2xl m-4 py-1 drop-shadow">{voyageData.voyageNom}</p>
                    <p className="w-3/4 flex items-center 2xl:text-lg m-1">Merci de remplir le formulaire suivant, sinon :</p>
                    <div 
                        className="w-1/2 bg-vertFonce rounded-2xl text-center font-semibold text-vertBlack mb-6 mr-6 mt-6 cursor-pointer 2xl:text-lg"
                        onClick={() => {
                            push(`/`)
                        }}
                    >
                        Retour à la liste des voayges
                    </div>
                    <div className="space-y-6  w-2/3 md:w-1/2 2xl:w-1/3">
                        <div>
                            <label className="block text-sm font-medium leading-6 text-gray-900">
                                Nom
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    required
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 indent-3"
                                    onChange={(e) => setNom(e.target.value)}
                                />
                            </div>
                        </div>

                        <div>
                            <label className="block text-sm font-medium leading-6 text-gray-900">
                                Prénom
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    required
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 indent-3"
                                    onChange={(e) => setPrenom(e.target.value)}
                                />
                            </div>
                        </div>

                        <div>
                            <label className="block text-sm font-medium leading-6 text-gray-900">
                                Email
                            </label>
                            <div className="mt-2">
                                <input
                                    type="email"
                                    required
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 indent-3"
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                            </div>
                        </div>

                        <div>
                            <label className="block text-sm font-medium leading-6 text-gray-900">
                                Téléphone
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    required
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 indent-3"
                                    onChange={(e) => setTel(e.target.value)}
                                />
                            </div>
                        </div>

                        <div>
                            <label className="block text-sm font-medium leading-6 text-gray-900">
                                Votre message :
                            </label>
                            <div className="mt-2">
                                <textarea 
                                    required
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 indent-3"
                                    onChange={(e) => setMessage(e.target.value)}
                                ></textarea>
                            </div>
                        </div>
                    </div>
                    <div className="w-full flex flex-row justify-evenly items-center mt-6">
                        <div 
                            className="w-1/6 bg-vertFonce rounded-2xl text-center font-semibold text-vertBlack mb-6 mr-6 mt-6 cursor-pointer 2xl:text-lg"
                            onClick={() => {
                                push(`/voyages/${voyageData.voyageNom}`)
                            }}
                        >
                                Retour
                        </div>
                        <div 
                            className="w-1/6 bg-vertFonce rounded-2xl text-center font-semibold text-vertBlack mb-6 mr-6 mt-6 cursor-pointer 2xl:text-lg"
                            onClick={() => {
                                push(`/`)
                            }}
                        >
                            Retour à l'accueil
                        </div>
                        <button
                            className="w-1/6 bg-vertFonce rounded-2xl text-center font-semibold text-vertBlack mb-6 mr-6 mt-6 cursor-pointer 2xl:text-lg"
                            onClick={() => handleSubmit()}
                        >
                            Envoyer
                        </button>
                    </div>

                </div>
            )}
            < Footer />
        </div>
    
    )
}

export default Page