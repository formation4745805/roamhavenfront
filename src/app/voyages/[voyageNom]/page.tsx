'use client'

import { Footer } from "@/app/Components/footer"
import { Header } from "@/app/Components/header"
import { getThisVoyageByName } from "@/app/Services/voyage"
import { showProps } from "@/app/Utils/types"
import { useRouter } from "next/navigation"
import { useEffect, useState } from "react"
import { MagnifyingGlass } from "react-loader-spinner"


const Page = ({ params }: { params: { voyageNom: string } }) => {

    const [voyageData, setVoyageData] = useState<showProps>()
    const [isLoading, setIsLoading] = useState(false)
    const {push} = useRouter()

    useEffect(() => {

        setIsLoading(true)

        getThisVoyageByName(params.voyageNom).then((res) => {
            setVoyageData(res)
            setIsLoading(false)
        })
    }, [])

    return (
        <div className="flex min-h-screen flex-col items-center justify-between bg-white">
            < Header />
            {isLoading && (
                <div className="w-full h-full flex items-center justify-center">
                    < MagnifyingGlass
                      visible={true}
                      height="80"
                      width="80"
                      ariaLabel="magnifying-glass-loading"
                      wrapperStyle={{}}
                      wrapperClass="magnifying-glass-wrapper"
                      glassColor="#c0efff"
                      color="#e15b64"
                      />
                </div>)}
            {voyageData && (
                <div
                key={voyageData.id}
                className="m-6 w-11/12 lg:w-2/3 2xl:w-10/12 rounded-md flex flex-col 2xl:flex-row items-center justify-center bg-blanchatre p-2 lg:p-4 shadow-noiratre"
                >
                    <img
                        src={voyageData.voyageImage}
                        className=" w-full 2xl:w-1/3 h-48 object-cover rounded-t-md cursor-pointer shadow-verte"
                    />
                    <div className="w-full 2xl:w-2/3 2xl:ml-6 flex flex-col justify-center items-center">
                        <h2 className="text-center font-bold text-2xl 2xl:text-3xl m-4 py-1 drop-shadow">
                            {voyageData.voyageNom}
                        </h2>
                        <div className="w-full flex flex-row items-center justify-between 2xl:justify-evenly m-6">
                            <p className="w-3/4">Départ le : {new Date(voyageData.voyageDebut).toLocaleDateString('FR')}</p>
                            <div className="bg-vertClaire rounded-2xl w-1/4 flex items-center justify-center text-vertBlack font-semibold 2xl:text-lg 2xl:font-bold">
                                {voyageData.voyageDuree} jours
                            </div>
                        </div>
                        <div className="bg-vertClaire rounded-2xl w-3/4 flex justify-center items-center text-vertBlack font-bold text-xl 2xl:text-2xl m-6">
                            {voyageData.voyagePrix} Berry /personne
                        </div>
                        <div className="w-3/4 flex items-center justify-center 2xl:text-lg m-1">
                            {voyageData.voyageDescription}
                        </div>
                        <div className="flex flex-row items-center justify-between 2xl:justify-evenly w-full m-6">
                            <div className="flex flex-col w-3/4">
                                <div className="flex flex-row">
                                {voyageData.roamhavenPays && (
                                Array.isArray(voyageData.roamhavenPays) ? (
                                    voyageData.roamhavenPays.map((pays: any) => (
                                        <div key={pays.paysNom} className="bg-jaunatre rounded-2xl text-vertFonce text-center text-sm m-1">
                                            {pays.paysNom}
                                        </div>
                                    ))
                                ) : (
                                    <div className="bg-jaunatre rounded-2xl text-vertFonce text-center text-sm m-1">
                                        {voyageData.roamhavenPays.paysNom}
                                    </div>
                                )
                            )}
                                </div>
                                <div className="flex flex-row">
                                {voyageData.roamhavenCategorie && (
                                Array.isArray(voyageData.roamhavenCategorie) ? (
                                    voyageData.roamhavenCategorie.map((categorie: any) => (
                                        <div key={categorie.categorieNom} className="bg-jaunatre rounded-2xl text-vertFonce text-center text-sm m-1">
                                            {categorie.categorieNom}
                                        </div>
                                    ))
                                ) : (
                                    <div className="bg-jaunatre rounded-2xl text-vertFonce text-center text-sm m-1">
                                        {voyageData.roamhavenCategorie.categorieNom}
                                    </div>
                                )
                            )}
                                </div>
                            </div>
                            <div className="w-1/4 flex flex-col">
                                <div 
                                    className="w-11/12 bg-vertFonce rounded-2xl text-center font-semibold text-vertBlack mt-6 mr-6 cursor-pointer 2xl:text-lg"
                                    onClick={() => {
                                        push(`/voyages/${voyageData.voyageNom}/formulaire/${voyageData.id}`)
                                    }}>
                                    Formulaire
                                </div>
                                <div 
                                    className="w-11/12 bg-vertFonce rounded-2xl text-center font-semibold text-vertBlack mb-6 mr-6 mt-6 cursor-pointer 2xl:text-lg"
                                    onClick={() => {
                                        push(`/`)
                                    }}>
                                    Retour
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
            < Footer />
        </div>
    )
}

export default Page