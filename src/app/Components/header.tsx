'use client'

export const Header = () => {

    return (
        <header className="bg-vertClaire h-44 w-full flex flex-row items-center justify-start p-2">
            <img className="w-1/4 md:w-1/6 2xl:w-1/12 mr-4" src="logo-roamhaven-removebg.png" alt="Logo de Roamhaven agence de voyage" />
            <h1 className="w-3/4 md:w-4/6 lg:w-8/12 flex items-center justify-center text-4xl md:text-5xl lg:text-6xl font-bold text-center"> RoamHaven </h1>
        </header>
    )
}