"use client";
import { useEffect, useState } from "react";
import { Footer } from "./Components/footer";
import { Header } from "./Components/header";
import {
  getAllVoyages,
  getAllVoyagesByCategorie,
  getAllVoyagesByDuree,
  getAllVoyagesByPays,
} from "./Services/voyage";
import { useRouter } from "next/navigation";
import { getAllPays } from "./Services/pays";
import { getAllCategories } from "./Services/categorie";
import { MagnifyingGlass } from "react-loader-spinner";
import { FcSearch } from "react-icons/fc";

export default function Home() {
  const [voyageList, setVoyageList] = useState([]);
  const [paysListe, setPaysListe] = useState([]);
  const [categorieListe, setCategorieListe] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [trie, setTrie] = useState("");
  const [donnee, setDonnee] = useState("");
  const { push } = useRouter();

  useEffect(() => {
    setIsLoading(true);

    getAllCategories().then((res: any) => {
      setCategorieListe(res);
      setIsLoading(false);
    });

    getAllPays().then((res: any) => {
      setPaysListe(res);
      setIsLoading(false);
    });

    if (trie == "pays") {
      getAllVoyagesByPays(donnee).then((res: any) => {
        setVoyageList(res);
        setIsLoading(false);
      });
    } else if (trie == "categorie") {
      getAllVoyagesByCategorie(donnee).then((res: any) => {
        setVoyageList(res);
        setIsLoading(false);
      });
    } else if (trie == "duree") {
      getAllVoyagesByDuree(donnee).then((res: any) => {
        setVoyageList(res);
        setIsLoading(false);
      });
    } else {
      getAllVoyages().then((res: any) => {
        setVoyageList(res);
        setIsLoading(false);
      });
    }
  }, [trie, donnee]);

  return (
    <div className="flex min-h-screen flex-col items-center justify-between bg-white">
      <Header />
      <div className="bg-banner bg-cover bg-center-top-4 lg:bg-center-top-8 w-full h-80 flex justify-center items-end p-4">
        <div className="bg-jaunatre h-8 w-11/12 md:w-3/4 xl:w-1/2 rounded-3xl flex flex-row justify-evenly items-stretch">
          <select
            onChange={(e) => {
              setTrie("pays");
              setDonnee(e.target.value);
            }}
            className="border-solid border-vertBlack border-2 flex-1 flex items-center justify-center rounded-l-3xl bg-jaunatre"
            defaultValue=""
          >
            <option className="bg-jaunatre" value="" disabled>
              Pays
            </option>
            {paysListe &&
              paysListe.map((pays: any) => {
                return <option value={pays.paysNom}>{pays.paysNom}</option>;
              })}
          </select>
          <select
            onChange={(e) => {
              setTrie("categorie");
              setDonnee(e.target.value);
            }}
            className="border-solid border-vertBlack border-y-2 border-r-2 flex-1 flex items-center justify-center bg-jaunatre"
            defaultValue=""
          >
            <option className="bg-jaunatre" value="" disabled>
              Catégorie
            </option>
            {categorieListe &&
              categorieListe.map((categorie: any) => {
                return (
                  <option value={categorie.categorieNom}>
                    {categorie.categorieNom}
                  </option>
                );
              })}
          </select>
          <select
            onChange={(e) => {
              setTrie("duree");
              setDonnee(e.target.value);
            }}
            className="border-solid border-vertBlack border-y-2 flex-1 flex items-center justify-center bg-jaunatre"
            defaultValue=""
          >
            <option className="bg-jaunatre" value="" disabled>
              Durée
            </option>
            <option value="3">3 jours</option>
            <option value="7">7 jours</option>
            <option value="14">14 jours</option>
          </select>
          <div
            onClick={() => {
              setTrie("");
              setDonnee("");
            }}
            className="border-solid border-vertBlack border-2 flex-1 flex items-center justify-center rounded-r-3xl cursor-pointer"
          >
            Tous
            <span>
              <FcSearch />
            </span>
          </div>
        </div>
      </div>
      <div className="flex items-center flex-wrap w-5/6 lg:w-11/12 my-4 justify-around">
        {isLoading && (
          <div className="w-full h-full flex items-center justify-center">
            <MagnifyingGlass
              visible={true}
              height="80"
              width="80"
              ariaLabel="magnifying-glass-loading"
              wrapperStyle={{}}
              wrapperClass="magnifying-glass-wrapper"
              glassColor="#c0efff"
              color="#e15b64"
            />
          </div>
        )}
        {voyageList &&
          voyageList.map((voyage: any) => {
            return (
              <div
                key={voyage.id}
                className="m-6 w-11/12 lg:w-5/12 2xl:w-3/12 rounded-md flex flex-col items-center bg-blanchatre p-2 shadow-noiratre"
              >
                <img
                  src={voyage.voyageImage}
                  className="w-full h-48 object-cover rounded-t-md cursor-pointer shadow-verte"
                  onClick={() => {
                    push(`/voyages/${voyage.voyageNom}`);
                  }}
                />
                <h2 className="text-center font-bold text-2xl m-4 py-1 drop-shadow">
                  {voyage.voyageNom}
                </h2>
                <div className="w-full flex flex-row items-center justify-between m-6">
                  <p className="w-3/4">
                    Départ le :{" "}
                    {new Date(voyage.voyageDebut).toLocaleDateString("FR")}
                  </p>
                  <div className="bg-vertClaire rounded-2xl w-1/4 flex items-center justify-center text-vertBlack font-semibold">
                    {voyage.voyageDuree} jours
                  </div>
                </div>
                <div className="bg-vertClaire rounded-2xl w-3/4 flex justify-center items-center text-vertBlack font-bold text-xl m-6">
                  {voyage.voyagePrix} Berry /personne
                </div>
                <div className="flex flex-row items-center justify-between w-full m-6">
                  <div className="flex flex-col w-3/4">
                    <div className="flex flex-row">
                      {voyage.roamhavenPays &&
                        voyage.roamhavenPays.map((pays: any) => {
                          return (
                            <div className="bg-jaunatre rounded-2xl text-vertFonce text-center text-sm m-1">
                              {pays.paysNom}
                            </div>
                          );
                        })}
                    </div>
                    <div className="flex flex-row">
                      {voyage.roamhavenCategorie &&
                        voyage.roamhavenCategorie.map((categorie: any) => {
                          return (
                            <div className="bg-jaunatre rounded-2xl text-vertFonce text-center text-sm m-1">
                              {categorie.categorieNom}
                            </div>
                          );
                        })}
                    </div>
                  </div>
                  <div
                    className="w-1/4 bg-vertFonce rounded-2xl text-center font-semibold text-vertBlack m-6 cursor-pointer"
                    onClick={() => {
                      push(`/voyages/${voyage.voyageNom}`);
                    }}
                  >
                    Détails
                  </div>
                </div>
              </div>
            );
          })}
      </div>
      <Footer />
    </div>
  );
}
