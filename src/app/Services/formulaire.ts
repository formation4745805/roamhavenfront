import axios from "axios"
import { formProps } from "../Utils/types"

export async function formulaireRegister(formProps: formProps) {
    let url = `${process.env.NEXT_PUBLIC_API_URL}api/formulaire/register`

    let axiosConfig = {
        headers: {
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        },
    }
    return axios
        .post(
            url,
            {
                userNom: formProps.nom,
                userPrenom: formProps.prenom,
                userEmail: formProps.email,
                userTel: formProps.tel,
                formulaireMessage: formProps.message,
            },
            axiosConfig
        )
        .then((res) => {
            return res
        })
}