import axios from "axios"

export async function getAllVoyages() {
    let axiosConfig = {
        headers: {
            'content-type': 'application/json',
        },
    }
    let url = `${process.env.NEXT_PUBLIC_API_URL}api/voyages`
    return axios.get(url, axiosConfig).then((res) => {
        return res.data
    })
}

export async function getAllVoyagesByPays(pays:string) {
    let axiosConfig = {
        headers: {
            'content-type': 'application/json',
        },
    }
    let url = `${process.env.NEXT_PUBLIC_API_URL}api/voyage/pays/${pays}`
    return axios.get(url, axiosConfig).then((res) => {
        return res.data
    })
}

export async function getAllVoyagesByCategorie(categorie:string) {
    let axiosConfig = {
        headers: {
            'content-type': 'application/json',
        },
    }
    let url = `${process.env.NEXT_PUBLIC_API_URL}api/voyage/categorie/${categorie}`
    return axios.get(url, axiosConfig).then((res) => {
        return res.data
    })
}

export async function getAllVoyagesByDuree(duree:string) {
    let axiosConfig = {
        headers: {
            'content-type': 'application/json',
        },
    }
    let url = `${process.env.NEXT_PUBLIC_API_URL}api/voyage/duree/${duree}`
    return axios.get(url, axiosConfig).then((res) => {
        return res.data
    })
}

export async function getThisVoyageByName(nom:string) {
    let axiosConfig = {
        headers: {
            'content-type': 'application/json',
        },
    }
    let url = `${process.env.NEXT_PUBLIC_API_URL}api/voyage/nom/${nom}`
    return axios.get(url, axiosConfig).then((res) => {
        return res.data
    })
}

export async function getThisVoyageById(id:string) {
    let axiosConfig = {
        headers: {
            'content-type': 'application/json',
        },
    }
    let url = `${process.env.NEXT_PUBLIC_API_URL}api/voyage/id/${id}`
    return axios.get(url, axiosConfig).then((res) => {
        return res.data
    })
}