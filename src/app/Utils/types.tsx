export type miniatureProps = {
    id : string,
    voyageNom : string,
    voyageDebut : string,
    voyageImage : string,
    voyageDuree : string,
    voyagePrix : string,
    roamhavenPays : {
        paysNom : string,
    }
    roamhavenCategorie : {
        categorieNom : string
    }
}

export type showProps = {
    id : string,
    voyageNom : string,
    voyageDescription : string,
    voyageDebut : string,
    voyageImage : string,
    voyageDuree : string,
    voyagePrix : string,
    roamhavenPays : {
        paysNom : string,
    }
    roamhavenCategorie : {
        categorieNom : string
    }
}

export type formProps = {
    nom : string,
    prenom : string,
    email : string,
    tel: string,
    message: string,
}